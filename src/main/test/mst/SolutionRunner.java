package mst;

import org.junit.Test;

/**
 * Created by jszklarz on 5/25/17.
 */
public class SolutionRunner {

    @Test
    public void run() {
        Solution mst = new Solution();
        int[][] graph = new int[][] {{0,3,0,12},{1,0,18,10},{0,18,0,5},{12,10,5,0}};
        int[][] result = mst.minimumSpanningTree(graph);
        printGraph(result);
    }

    private void printGraph(int[][] mst) {
        for (int i = 0; i < mst.length; i++) {
            int[] node = mst[i];
            for (int j = 0; j < node.length; j++) {
                int edge = node[j];
                System.out.print(edge);
                if (j < node.length - 1) {
                    System.out.print(", ");
                }
            }
            System.out.println();
        }
    }
}
