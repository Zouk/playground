package minmaxsum;

import java.math.BigInteger;

public class Solution {

    private static int max = Integer.MIN_VALUE;
    private static int min = Integer.MAX_VALUE;
    private static int maxIndex = 0;
    private static int minIndex = 0;

    public static void minMax(int[] args) {
        int[] arr = new int[5];
        for(int arr_i=0; arr_i < args.length; arr_i++){
            int currentInt = args[arr_i];
            arr[arr_i] = currentInt;
            if (currentInt > max) {
                max = currentInt;
                maxIndex = arr_i;
            }
            if (currentInt < min) {
                min = currentInt;
                minIndex = arr_i;
            }
        }

        BigInteger minSum = BigInteger.ZERO;
        BigInteger maxSum = BigInteger.ZERO;

        for(int arr_i=0; arr_i < arr.length; arr_i++){
            if (arr_i != maxIndex) {
                minSum = minSum.add(new BigInteger(String.valueOf(arr[arr_i])));
            }
            if (arr_i != minIndex) {
                maxSum = maxSum.add(new BigInteger(String.valueOf(arr[arr_i])));
            }
        }

        System.out.println(minSum + " " + maxSum);
    }
}
