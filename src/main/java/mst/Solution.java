package mst;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jszklarz on 5/25/17.
 */
public class Solution {

    public int[][] minimumSpanningTree(int[][] graph) {
        //select start node (let's always choose node 0)
        int[][] mst = new int[graph.length][graph[0].length];
        List<Integer> notVisited = getNodeList(graph.length);
        List<Integer> visited = new ArrayList<>();

        int currentNode = 0;
        visited.add(currentNode);
        notVisited.remove((Integer) currentNode);
        while(!notVisited.isEmpty()) {
            VertexConnection nextConnection = getNextNearestNode(graph, visited);
            notVisited.remove((Integer) nextConnection.nodeB);
            visited.add(nextConnection.nodeB);
            mst[nextConnection.nodeA][nextConnection.nodeB] = nextConnection.edgeWeight;
        }

        return mst;
    }

    private VertexConnection getNextNearestNode(int[][] graph, List<Integer> visited) {
        int minimumWeight = Integer.MAX_VALUE;
        int nodeAlreadyVisited = -1;
        int nextNodeToVisit = -1;

        for (int node : visited) {
            for (int connNode = 0; connNode < graph[node].length; connNode++) {
                int edge = graph[node][connNode];
                if (!visited.contains(connNode) && edge > 0 && edge < minimumWeight) {
                    minimumWeight = edge;
                    nodeAlreadyVisited = node;
                    nextNodeToVisit = connNode;
                }
            }
        }

        return new VertexConnection(nodeAlreadyVisited, nextNodeToVisit, minimumWeight);
    }

    private List<Integer> getNodeList(int length) {
        List<Integer> nodeList = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            nodeList.add(i);
        }
        return nodeList;
    }

    public class VertexConnection {
        int nodeA;
        int nodeB;
        int edgeWeight;

        public VertexConnection(int nodeA, int nodeB, int edgeWeight) {
            this.nodeA = nodeA;
            this.nodeB = nodeB;
            this.edgeWeight = edgeWeight;
        }
    }
}
